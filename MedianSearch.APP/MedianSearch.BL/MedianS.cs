﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MedianSearch.BL
{
    public class MedianS
    {
        public int Median { get; private set; }

        public void Search(int[] array, int first, int last)
        {
            int k = array.Length / 2;
            int p = array[first];
            int i, j;
            do
            {
                for (i = first; i <= last; i++)
                {
                    if (array[i] >= p)
                        break;
                }
                for (j = last; j >= first; j--)
                {
                    if (array[j] <= p)
                        break;
                }
                Swap(ref array[i], ref array[j]);
                if (array[i] == array[j])
                    i++;
            } while (i < j);
            array[j] = p;
            if (j > k)
                Search(array, first, j - 1);
            else if (j < k)
                Search(array, j + 1, last);
            else if (j == k)
                Median = array[j];
        }

        private void Swap(ref int a, ref int b)
        {
            a ^= b;
            b ^= a;
            a ^= b;
        }
    }
}
