﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MedianSearch.BL;

namespace MedianSearch.APP
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr;
            int length;
            Random random = new Random();
            MedianS median = new MedianS();

            do
            {
                Console.WriteLine("Array length: ");
                length = Convert.ToInt32(Console.ReadLine());
                arr = new int[length];
                Console.WriteLine("Generated array:");
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = random.Next(100);
                    Console.Write($"{arr[i]} ");
                }
                median.Search(arr, 0, arr.Length - 1);
                Console.WriteLine($"\nMedian is: {median.Median}\n");

            } while (true);

        }
    }
}
