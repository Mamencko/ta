﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heap
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr;
            int[] arr1;
            int length;
            Random random = new Random();
            HeapC heap = new HeapC();

            do
            {
                Console.Write("Array length: ");
                length = Convert.ToInt32(Console.ReadLine());
                arr = new int[length + 1];
                arr1 = new int[length + 1];
                Console.WriteLine("Generated array:");
                for (int i = 1; i < arr1.Length; i++)
                {
                    arr1[i] = random.Next(100);
                    Console.Write($"{arr1[i]} ");
                }
                heap.HeapBottomUp(arr1);
                Console.WriteLine("Heap array:");
                for (int i = 1; i < arr1.Length; i++)
                {
                    Console.Write($"{arr1[i]} ");
                }
                heap.DeleteRoot(ref arr1);
                Console.WriteLine("Deleted root:");
                for (int i = 1; i < arr1.Length; i++)
                {
                    Console.Write($"{arr1[i]} ");
                }
                Console.WriteLine("/nGenerated array:");
                for (int i = 1; i < arr.Length; i++)
                {
                    arr[i] = random.Next(100);
                    Console.Write($"{arr[i]} ");
                }
                heap.HeapSort(arr);
                Console.WriteLine("\nSorted array:");
                for (int i = 1; i < arr.Length; i++)
                {
                    Console.Write($"{arr[i]} ");
                }
                Console.WriteLine("\n");
            } while (true);
        }
    }
}
