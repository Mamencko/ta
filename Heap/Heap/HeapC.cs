﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Heap
{
    public class HeapC
    {
        public void HeapBottomUp(int[] array)
        {
            int j, k, v, n = array.Length - 1;
            bool heap;

            for (int i = n / 2; i >= 1; i--)
            {
                k = i;
                v = array[k];
                heap = false;
                while (!heap && 2 * k <= n)
                {
                    j = 2 * k;
                    if (j < n)
                        if (array[j] < array[j + 1])
                            j++;
                    if (v >= array[j])
                        heap = true;
                    else
                    {
                        array[k] = array[j];
                        k = j;
                    }
                }
                array[k] = v;
            }
        }

        public int DeleteRoot(ref int[] arr)
        {
            int tmp = arr[1];
            arr[1] = arr[arr.Length - 1];
            arr[arr.Length - 1] = tmp;

            Array.Resize<int>(ref arr, arr.Length - 1);

            HeapBottomUp(arr);

            return tmp;
        }

        public void HeapSort(int[] array)
        {
            int[] arr = array;

            HeapBottomUp(arr);

            for (int i = 1; i < array.Length; i++)
            {
                array[i] = DeleteRoot(ref arr);
                HeapBottomUp(arr);
            }
        }
    }
}